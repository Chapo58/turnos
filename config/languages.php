<?php

return [
    'en_US' => 'English',
    'es_AR' => 'Español',
    'it_IT' => 'Italiano',
    'fr_FR' => 'Français',
];
