<?php

return  [
  //==================================== Translations ====================================//
  'feature' => [
    'customized_support'     => 'Soporte Personalizado',
    'one_specialist'         => '<strong>Un</strong> especialista',
    'unlimited_appointments' => 'Turnos <strong>sin límite</strong>',
    'unlimited_contacts'     => 'Agenda de clientes <strong>sin límite</strong>',
    'unlimited_services'     => 'Servicios <strong>sin límite</strong>',
    'unlimited_specialists'  => 'Especialistas <strong>sin límite</strong>',
    'up_to_contacts'         => 'Agenda de clientes <strong>hasta :limit</strong>',
    'up_to_services'         => 'Servicios <strong>hasta :limit</strong>',
  ],
  'free'  => 'GRATIS',
  'month' => 'mes',
  'plan'  => [
    'free' => [
      'hint'   => 'Ideal para probar el servicio',
      'name'   => 'Free',
      'submit' => 'Empezar',
    ],
    'premium' => [
      'hint'   => 'Ideal para negocios',
      'name'   => 'Premium',
      'submit' => 'Contratar',
    ],
  ],
];
