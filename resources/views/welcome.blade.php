<!DOCTYPE html>
<html lang="es">
<head>
<!-- /.website title -->
<title>{{trans('app.name')}}</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />

<!-- CSS Files -->
<link href="{{ asset('css/welcome/bootstrap.min.css') }}" rel="stylesheet" media="screen">
<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/welcome/animate.css') }}" rel="stylesheet" media="screen">

<link href="{{ asset('css/welcome/styles.css') }}" rel="stylesheet" media="screen">

</head>

<body data-spy="scroll" data-target="#navbar-scroll">

 <div id="top"></div>

<!-- /.parallax full screen background image -->
<div class="fullscreen landing parallax" style="background-image:url('images/bg.jpg');" data-img-width="2000" data-img-height="1325" data-diff="100">

	<div class="overlay">
		<div class="container">
			<div class="row">
				<div class="col-md-6">

						<!-- /.main title -->
						<h1 class="wow fadeInRight">
						Turnos Online
						</h1>

					<!-- /.header paragraph -->
					<div class="landing-text wow fadeInRight">
						<p>La agenda de citas para profesionales exitosos!</p>
					</div>

					<!-- /.header button -->



					<!-- /.phone option -->
					<div class="more wow fadeInRight">
						<a href="{{ url('/register') }}" style="background-color:#4CAF50;" class="btn option"><i class="fa fa-plus-circle"></i>Soy Nuevo</a>
						<a href="{{ url('/login') }}" class="btn option"><i class="fa fa-paper-plane"></i>Ya tengo Cuenta!</a>
						</p>
					</div>

				</div>

				<!-- /.phone image -->
				<div class="col-md-6">
				<img src="{{ asset('img/calendario.png') }}" alt="Calendario" class="header-phone img-responsive wow fadeInLeft">
				</div>



			</div>
		</div>
	</div>
</div>

<!-- NAVIGATION -->
<div id="menu">
	<nav class="navbar-wrapper navbar-default" role="navigation">
		<div class="container">
			  <div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-themers">
				  <span class="sr-only">Toggle navigation</span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				</button>
				<a class="navbar-brand site-name" href="#top"><img src="{{ asset('img/logo.png') }}" alt="LOGO"></a>
			  </div>

			  <div id="navbar-scroll" class="collapse navbar-collapse navbar-themers navbar-right">
				<ul class="nav navbar-nav">
					<li><a href="#top">Inicio</a></li>
					<li><a href="#intro">Info</a></li>
					<li><a href="#feature">¿Por que usarlo?</a></li>
				</ul>
			  </div>
		</div>
	</nav>
</div>

<!-- /.intro section -->
<div id="intro">
	<div class="container">
		<div class="row">
			<!-- /.intro content -->
			<div class="col-md-12 wow slideInRight">
				<h2>Sistema de gestión de turnos online</h2>
				<p>Tu tiempo vale! Ahorra tiempo solicitando tus turnos de manera online en tan solo un par de pasos.</p>

					<div class="btn-section"><a href="#feature" class="btn-default">Ver mas</a></div>

			</div>
		</div>
	</div>
</div>

<!-- /.feature section -->
<div id="feature">
	<div class="container">
		<div class="row row-feat">

			<div class="col-md-12">

				<!-- /.feature 1 -->
				<div class="col-sm-3 feat-list">
					<i class="fa fa-calendar-check-o wow fadeInUp"></i>
					<div class="inner">
						<h4>{{trans('welcome.feature.1.title')}}</h4>
						<p>{{trans('welcome.feature.1.content')}}</p>
					</div>
				</div>

				<!-- /.feature 2 -->
				<div class="col-sm-3 feat-list">
					<i class="fa fa-user-plus wow fadeInUp" data-wow-delay="0.2s"></i>
					<div class="inner">
						<h4>{{trans('welcome.feature.2.title')}}</h4>
						<p>{{trans('welcome.feature.2.content')}}</p>
					</div>
				</div>

				<!-- /.feature 3 -->
				<div class="col-sm-3 feat-list">
					<i class="fa fa-briefcase wow fadeInUp" data-wow-delay="0.4s"></i>
					<div class="inner">
						<h4>{{trans('welcome.feature.3.title')}}</h4>
						<p>{{trans('welcome.feature.3.content')}}</p>
					</div>
				</div>

				<!-- /.feature 4 -->
				<div class="col-sm-3 feat-list">
					<i class="fa fa-hourglass-start wow fadeInUp" data-wow-delay="0.4s"></i>
					<div class="inner">
						<h4>{{trans('welcome.feature.4.title')}}</h4>
						<p>{{trans('welcome.feature.4.content')}}</p>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<!-- /.footer -->
<footer id="footer">
	<div class="container">
		<div class="col-sm-4 col-sm-offset-4">
			<!-- /.social links -->
				<div class="social text-center">
					<ul>
						<li><a class="wow fadeInUp" href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a class="wow fadeInUp" href="#" data-wow-delay="0.2s"><i class="fa fa-facebook"></i></a></li>
						<li><a class="wow fadeInUp" href="#" data-wow-delay="0.4s"><i class="fa fa-google-plus"></i></a></li>
						<li><a class="wow fadeInUp" href="#" data-wow-delay="0.6s"><i class="fa fa-instagram"></i></a></li>
					</ul>
				</div>
			<div class="text-center wow fadeInUp" style="font-size: 14px;">Copyright <a target="_blank" href="http://www.artisan.com.ar">Artisan</a> 2017</div>
			<a href="#" class="scrollToTop"><i class="pe-7s-up-arrow pe-va"></i></a>
		</div>
	</div>
</footer>

	<!-- /.javascript files -->
    <script src="{{ asset('js/welcome/jquery.js') }}"></script>
    <script src="{{ asset('js/welcome/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/welcome/custom.js') }}"></script>
	<script src="{{ asset('js/welcome/jquery.sticky.js') }}"></script>
	<script src="{{ asset('js/welcome/wow.min.js') }}"></script>
	<script src="{{ asset('js/welcome/owl.carousel.min.js') }}"></script>
	<script>
		new WOW().init();
	</script>
  </body>
</html>
