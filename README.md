1) composer install

2) Crear base de datos en formato utf8_general_ci

3) Configurar .env

4) php artisan key:generate

5) php artisan migrate

6) php artisan db:seed

7) php artisan geoip:update (Cambiar en .ENV CACHE_DRIVER=array) 

8) 
php.conf quitar comentario a:
;extension=php_intl.dll

9) php artisan serve

Para la demo:

10) php artisan db:seed --class=TestingDatabaseSeeder

USER: demo@timegrid.io
PASS: demomanager

USER: guest@example.org
PASS: demoguest